import React, {useState} from 'react';

const Car = () => {
    // const [currentImg, setCurrentImg] = useState("");
    const [currentCarName, setCurrentCarName] = useState("");
    // const [currentCarPrice, setCurrentCarPrice] = useState(0);
    // const [currentCarInfo, setCurrentCarInfo] = useState("");

    const [userPrice, setUserPrice] = useState("");
    const [error ,setError] = useState("");


    const carList = [
        {
            img : "이미지1",
            carName : "모닝",
            carPrice : 10000000,
            carInformation : "모던한 City Look 디자인과 도심에 최적화된 상품성으로\n 더 가볍고 활기찬 어반 라이프"
        },
        {
            img : "이미지1",
            carName : "레이",
            carPrice : 20000000,
            carInformation : "다채로운 라이프를 선사하는 모빌리티"
        },
        {
            img : "이미지1",
            carName : "K3",
            carPrice : 30000000,
            carInformation : "역동적인 디자인과\n운전자를 배려하는 다양한 편의사양"
        },
        {
            img : "이미지1",
            carName : "K3 GT",
            carPrice : 40000000,
            carInformation : "일상의 즐거움도 놓치지 않는다"
        },
        {
            img : "이미지1",
            carName : "K5",
            carPrice : 50000000,
            carInformation : "모가슴 뛰는 디자인과\n감각적인 혁신으로 완성한\n가장 진보적인 세단"
        },
        {
            img : "이미지1",
            carName : "K8",
            carPrice : 60000000,
            carInformation : "The Outstanding, 영감은 낯선 곳으로 부터"
        },
        {
            img : "이미지1",
            carName : "K9",
            carPrice : 70000000,
            carInformation : "Masters that Inspire"
        }
    ]

     // sort 로 내림차순 정렬



    // 입력받은 돈에서 7%,30만원를 빼고 얼마가 나오는지 만원단위로 리턴해줌
    const Judgment = (payPrice = 0) => {
        const newPrice = (userPrice - 400000) / 1.07
        return Math.ceil(newPrice / 10000) * 10000
    }




    // 실제로 유저가 입력한 값만 배낼 수 있음
    const handleUserPriceChange = e => {
        setUserPrice(e.target.value)
        setCurrentCarName('')
        setError('')
    }

    // 원하는 게
    //
    // 필터된 가격이 다 한 번 씩 비교해서 돌리는데
    //
    // 필터된 가격 - 돌리는 자동차의 가격이
    //
    // 음수면 -1 번째 꺼 리턴
    //
    // 양수면 반복

// TwoTurn 함수에서 찾은 차량 이름을 설정하는 대신 반환하도록 변경
//     const TwoTurn = (Price) => {
//         let filter = Judgment(Price)
//
//         for (let i = 0; i < carList.length; i ++) {
//             let minus = filter - carList[i].carPrice
//             if (minus <= 0) {
//                 return carList[i].carName;
//             }
//         }
//     }

    const calculateCar = () => {
        let filter = Judgment(userPrice)
        console.log(filter)
        if (userPrice < carList[0].carPrice) {
             setError("중고차 사러 가세용 *^_^*")
        } else {
            // 필터랑 소트로 if
            // 로직 -> 필터링한 가격을 포문으로 돌려라 언제까지 음수가 될 때 까지 .. 음수되면 리턴 전 꺼 .,.
        }
    }

    return(
        <div>
            <input type="number" value={userPrice} onChange={handleUserPriceChange}/>
            <button onClick={calculateCar}>추천받기</button>
            {error ? <p>{error}</p> : <p>추천차량 : {currentCarName}</p>}
        </div>
    )
}

export default Car;